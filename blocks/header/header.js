var searchBtn = document.getElementById('searchBtn');
var searchCloseBtn = document.getElementById('searchClose');
searchBtn.addEventListener('click', function(e) {
  e.preventDefault();
  document.getElementsByTagName('body')[0].classList.toggle('searchOpen');
});
searchCloseBtn.addEventListener('click', function() {
  document.getElementsByTagName('body')[0].classList.remove('searchOpen');
});
