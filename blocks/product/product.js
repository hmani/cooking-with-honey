var colorPicker = document.getElementsByClassName('colorPicker');
var product = document.getElementById('productBuy');
var colorImages = document.getElementsByClassName('colordep');
var colorThumbs = document.getElementsByClassName('colordepthumbs');
var allimages = document.getElementsByClassName('product_photo_main');
var allthumbs = document.getElementsByClassName('product_photo_thumb');
var basket = document.getElementById('basket');

var addToBasket = document.getElementById('addBasket');
var productForm = document.getElementById('productForm');
var continueShopping = document.getElementById('continueShopping');

for (var i = 0; i < colorPicker.length; i++) {
  colorPicker[i].addEventListener('click', function() {

//this no longer needed, controlled through sibling selectors!!
    // //remove all active classes
    // for (var i = 0; i < colorPicker.length; i++) {
    //   colorPicker[i].classList.remove('active');
    // }
    //
    // //add active class to clicked item
    // this.classList.add('active');


//check for stock change classes accordingly
        if (!(JSON.parse(this.dataset.stock))) {
      product.classList.add('nostock');
      document.getElementById('quantity').disabled = true;
    } else {
      product.classList.remove('nostock');
      document.getElementById('quantity').disabled = false;
    }

    //get correct color image
    let color = this.dataset.color;
    document.getElementById('selected-color').innerHTML = color;
    // console.log(color);

    //remove active classes from thumnbs and main image
    for (var i = 0; i < allimages.length; i++) {
      allimages[i].classList.remove('active');
    }
    for (var i = 0; i < allthumbs.length; i++) {
      allthumbs[i].classList.remove('active');
    }

    //add active class to correct image
    for (var i = 0; i < colorImages.length; i++) {
      if (colorImages[i].dataset.color === color) {
        colorImages[i].classList.add('active');
      }
    }

    //show currect color thumb, hide incorrect colored thumbs, make correct colored thumb active
    for (var i = 0; i < colorThumbs.length; i++) {
      if (colorThumbs[i].dataset.color === color) {
        colorThumbs[i].classList.add('visible');
        colorThumbs[i].classList.add('active');
      } else {
        colorThumbs[i].classList.remove('visible');

      }
    }
  });
};


//image gallery
for (var i = 0; i < allthumbs.length; i++) {
  allthumbs[i].addEventListener('click', function() {
    for (var i = 0; i < allimages.length; i++) {
      allimages[i].classList.remove('active');
    }
    for (var i = 0; i < allthumbs.length; i++) {
      allthumbs[i].classList.remove('active');
    }
    var target = this.dataset.target;
    document.getElementById(target).classList.add('active');
    this.classList.add('active');
  });
};

productForm.addEventListener('submit', function(e) {
  e.preventDefault();
  document.documentElement.classList.add('proceed');
  basket.classList.add('items');
  var add = Number(document.getElementById('quantity').value);
  var current = Number(basket.dataset.items) || 0;
  basket.dataset.items = current + add;
  // alert(current + add);
});
continueShopping.addEventListener('click', function(e) {
  e.preventDefault();
  document.documentElement.classList.remove('proceed');
});
