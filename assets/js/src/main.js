



//check view to see if elemnets in view,  Throttled.
window.addEventListener('scroll', throttle(checkView, 10));
window.addEventListener('load', function() {
  checkView();
});

var toCheckView = document.querySelectorAll('.transitions');

function checkView() {

    for (var i = 0; i < toCheckView.length; i++) {

        if (isElementInViewport(toCheckView[i])) {
            toCheckView[i].classList.add("visible");
            toCheckView[i].classList.add("currently");
        } else {
            toCheckView[i].classList.remove("currently");
        }
    }
}

function isElementInViewport(el) {
    var viewport = {};
    viewport.top = window.pageYOffset || (document.documentElement || document.body.parentNode || document.body).scrollTop;
    viewport.bottom = viewport.top + window.innerHeight;
    var middle = el.offsetTop + (el.clientHeight / 2);
    // console.log(viewport);
    return ((middle <= viewport.bottom) && (middle >= viewport.top));

}

//and check distance scrolled...
window.addEventListener('scroll', throttle(checkScroll, 10));
window.addEventListener('load', function() {
  checkScroll();
});

function checkScroll() {
    var distanceScrolled = window.pageYOffset || (document.documentElement || document.body.parentNode || document.body).scrollTop;
    if (distanceScrolled >= 120) {
        document.documentElement.classList.add("scrolled");
    } else {
        document.documentElement.classList.remove("scrolled");
    }
};



//throttle function
function throttle(fn, wait) {
    var time = Date.now();
    return function() {
        if ((time + wait - Date.now()) < 0) {
            fn();
            time = Date.now();
        }
    }
};

function index(el) {
    var children = el.parentNode.childNodes,
        i = 0;
    for (; i < children.length; i++) {
        if (children[i] == el) {
            return i;
        }
    }
    return -1;
};
