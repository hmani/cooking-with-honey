var gulp = require('gulp'),
    notify = require("gulp-notify"),
    clean = require('gulp-clean'),
    babel = require('gulp-babel'),
    concat = require('gulp-concat-util'),
    autoprefix = require('gulp-autoprefixer'),
    connect = require('gulp-connect'),
    inject = require('gulp-inject'),
    uglify = require('gulp-uglify'),
    plumber = require('gulp-plumber'),
    sass = require('gulp-sass'),
    gutil = require('gulp-util'),
    rename = require("gulp-rename"),
    pkg = require('./package.json'),
    pug = require('gulp-pug'),
    flatten = require('gulp-flatten'),
    prettify = require('gulp-prettify'),
    browserSync = require("browser-sync"),
    sassglob = require("gulp-sass-glob"),
    reload = browserSync.reload,
    argv = require('yargs').argv,
    file = require('gulp-file'),
    injectString = require('gulp-inject-string'),
    gulpPugBeautify = require('gulp-pug-beautify'),
    folderIsNamed = (argv.name === undefined) ? false : true;



gulp.task('connect', function() {
    connect.server({
        root: './',
        livereload: true
    });
});

var reportError = function(error) {
    var lineNumber = (error.lineNumber) ? 'LINE ' + error.lineNumber + ' -- ' : '';

    notify({
        title: 'Task Failed [' + error.plugin + ']',
        message: lineNumber + 'See console.',
        sound: 'Sosumi'
    }).write(error);

    gutil.beep();

    var report = '';
    var chalk = gutil.colors.white.bgRed;
    report += chalk('TASK:') + ' [' + error.plugin + ']\n';
    report += chalk('PROB:') + ' ' + error.message + '\n';
    if (error.lineNumber) {
        report += chalk('LINE:') + ' ' + error.lineNumber + '\n';
    }
    if (error.fileName) {
        report += chalk('FILE:') + ' ' + error.fileName + '\n';
    }
    console.error(report);

    // Prevent the 'watch' task from stopping
    this.emit('end');
}


gulp.task('delete', function() {
    return gulp.src(['./assets/js/**.js', './assets/css/**.css'], {
            read: false
        })
        .pipe(clean());
});

gulp.task('cleancss', function() {
    return gulp.src('./assets/css/**.css', {
            read: false
        })
        .pipe(clean());
});


//optionally create mini html templates for each 'block'

// gulp.task('blocks', function() {
//     gulp.src(['./blocks/**/*.pug'])
//         .pipe(plumber({
//             errorHandler: reportError
//         }))
//         .pipe(pug())
//         .pipe(flatten())
//         .pipe(prettify())
//         .pipe(gulp.dest('./elements/'));
// });


//main pug task
gulp.task('pug', function() {
    gulp.src(['index.pug'])
        .pipe(plumber({
            errorHandler: reportError
        }))
        .pipe(pug())
        .pipe(prettify())
        .pipe(gulp.dest('./'));
});



// JS concat and minify all plugins
gulp.task('plugins', function() {
    return gulp.src('./assets/js/plugins/*.js')
        .pipe(plumber({
            errorHandler: reportError
        }))
        // .pipe(uglify())
        .pipe(concat(pkg.name + '.plugins.js'))
        .pipe(gulp.dest('./assets/js'))
        .pipe(notify({
            message: 'JS plugins processed!'
        }));
});


//concats all scripts in 'blocks'
gulp.task('blockjs', function() {
    return gulp.src('./blocks/**/*.js')
        .pipe(plumber({
            errorHandler: reportError
        }))
        .pipe(concat('blocks.js'))
        .pipe(gulp.dest('./assets/js/src'))
        .pipe(notify({
            message: 'Block js concatted!'
        }));
});


//concats with main.js, runs all scripts through babel, wraps scripts in DOMContentLoaded event listener function...
gulp.task('scripts', function() {
    return gulp.src('./assets/js/src/*.js')
        .pipe(plumber({
            errorHandler: reportError
        }))
        .pipe(concat(pkg.name + '.main.js'))
        .pipe(concat.header('document.addEventListener("DOMContentLoaded", function() {\n'))
        .pipe(concat.footer('\n});\n'))
        .pipe(babel({
            presets: ['es2015']
        }))
        // .pipe(uglify())
        .pipe(gulp.dest('./assets/js'))
        .pipe(notify({
            message: 'ES2015 compiled and main scripts concatted'
        }));
});

// CSS concat, auto-prefix and minify
gulp.task('sass', function() {
    return gulp.src(['./assets/scss/style.scss'])
        .pipe(plumber({
            errorHandler: reportError
        }))
        .pipe(sassglob())
        .pipe(sass({
            style: 'expanded',
            includePaths: ['./assets/scss/partials', './assets/scss/modules', './assets/scss/vendor', './assets/scss/bower_components'],
            errLogToConsole: true
        }))
        .pipe(autoprefix('last 6 version'))
        .pipe(rename(pkg.name + '.css'))
        .pipe(gulp.dest('assets/css'))
        .pipe(notify({
            message: 'SCSS processed!'
        }));
});






//Default task. Local server. Run taks, watch, and run tasks on changes...
gulp.task('default', ['sass', 'plugins', 'blockjs', 'scripts', 'pug'], function() {
    browserSync({
        server: "./"
    });
    gulp.watch(['assets/scss/**/*.scss', 'blocks/**/*.scss'], ['sass']);
    gulp.watch('blocks/**/*.js', ['blockjs']);
    gulp.watch('assets/js/src/*.js', ['scripts']);
    gulp.watch('assets/js/plugins/*.js', ['plugins']);
    gulp.watch(['blocks/**/*.pug', 'index.pug', 'site.pug'], ['pug']);
    gulp.watch(['*.html', 'assets/js/' + pkg.name + ".main.js", 'assets/js/' + pkg.name + ".plugins.js", 'assets/css/' + pkg.name + ".css", ]).on('change', reload);
});

//Setup. Runs sass and script tasks, injects sources named from package name.
gulp.task('setup', ['delete', 'sass', 'plugins', 'scripts'], function() {
    (function() {
        var target = gulp.src('./blocks/head.html');
        var sources = gulp.src(['./assets/css/' + pkg.name + '.css'], {
            read: false

        });
        return target.pipe(inject(sources))
            .pipe(gulp.dest('blocks'))
            .pipe(connect.reload());
    })();
    (function() {
        var target = gulp.src('./blocks/scripts.html');
        var sources = gulp.src(['./assets/js/' + pkg.name + '.main.js', './assets/js/' + pkg.name + '.plugins.js'], {
            read: false
        });
        return target.pipe(inject(sources))
            .pipe(gulp.dest('blocks'))
            .pipe(connect.reload());
    })();
});

//Section task. Creates new 'block' using 'gulp section --name <name>'. A pug, scss, and js file will be created for the section.
gulp.task('section', function() {
    console.log(folderIsNamed);
    if (folderIsNamed) {
        var name = argv.name.toString();
        gulp.src('site.pug')
            .pipe(injectString.append('\n' + 'include blocks/' + name + '/' + name + '.pug'))
            .pipe(gulpPugBeautify())
            .pipe(gulp.dest('./'));
        console.log(name);
        return file([{
                name: name + ".js",
                source: ""
            }, {
                name: name + ".pug",
                source: ""
            }, {
                name: name + ".scss",
                source: ""
            }], {
                src: true
            })
            .pipe(gulp.dest("blocks/" + name))
    }
});
